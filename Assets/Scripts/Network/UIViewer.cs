using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIViewer : MonoBehaviour
{
	[SerializeField]
	private Button buttonStartServer;
	[SerializeField]
	private Button buttonShutDownServer;
	[SerializeField]
	private Button buttonConnectClient;
	[SerializeField]
	private Button buttonDisconnectClient;
	[SerializeField]
	private Button buttonSendMessage;
	[SerializeField]
	private TMP_InputField inputField;
	[SerializeField]
	private TextField textField;
	[SerializeField]
	private Server server;
	[SerializeField]
	private Client client;
	private void Start()
	{
		buttonStartServer.onClick.AddListener(() => StartServer());
		buttonShutDownServer.onClick.AddListener(() => ShutDownServer());
		buttonConnectClient.onClick.AddListener(() => Connect());
		buttonDisconnectClient.onClick.AddListener(() => Disconnect());
		buttonSendMessage.onClick.AddListener(() => SendMessage());
		client.onMessageReceive += ReceiveMessage;
		buttonDisconnectClient.enabled = false;
		buttonShutDownServer.enabled = false;
	}
	private void StartServer()
	{
		server.StartServer();
		buttonShutDownServer.enabled = true;
		buttonStartServer.enabled = false;

	}
	private void ShutDownServer()
	{
		server.ShutDownServer();
		buttonStartServer.enabled = true;
		buttonShutDownServer.enabled = false;
	}
	private void Connect()
	{
		client.Connect();
		buttonDisconnectClient.enabled = true;
		buttonConnectClient.enabled = false;
	}
	private void Disconnect()
	{
		client.Disconnect();
		buttonDisconnectClient.enabled = false;
		buttonConnectClient.enabled = true;
	}
	private void SendMessage()
	{
		client.SendMessage(inputField.text);
		inputField.text = "";
	}
	public void ReceiveMessage(object message)
	{
		textField.ReceiveMessage(message);
	}
}