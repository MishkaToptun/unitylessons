using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;

public partial class MyJobs : MonoBehaviour
{
	// Start is called before the first frame update
	private NativeArray<int> intsArray;
	void Start()
	{



		StartJob();

	}
	public void StartJob()
	{
		intsArray = new NativeArray<int>(10, Allocator.Persistent);
		for (int i = 0; i < intsArray.Length; i++)
		{
			intsArray[i] = Random.Range(0, 15);
		}
		TestJob job = new TestJob
		{
			jobArray = intsArray
		};
		var handle = job.Schedule();
		handle.Complete();
		NativeArray<Vector3> positions = new NativeArray<Vector3>(10, Allocator.Persistent);
		NativeArray<Vector3> velocities = new NativeArray<Vector3>(10, Allocator.Persistent);
		for (int i = 0; i < positions.Length; i++)
		{
			positions[i] = (Random.insideUnitSphere * 10F) * Random.Range(0, 110);
			velocities[i] = (Random.insideUnitSphere * 10F) * Random.Range(0, 110);
		}
		StartVectorJob(positions, velocities);

	}

	private static void StartVectorJob(NativeArray<Vector3> positions, NativeArray<Vector3> velocities)
	{
		Debug.Log("Start VectorJob");
		NativeArray<Vector3> newPositions = new NativeArray<Vector3>(positions.Length, Allocator.Persistent);
		MyVectorJob vectorJob = new MyVectorJob
		{
			positions = positions,
			velocities = velocities,
			finalPositions = newPositions
		};
		var h = vectorJob.Schedule(positions.Length, 5);
		h.Complete();

		for (int i = 0; i < vectorJob.finalPositions.Length; i++)
		{
			Debug.Log(newPositions[i]);
		}

		positions.Dispose();
		velocities.Dispose();
		newPositions.Dispose();
	}


	private void OnDestroy()
	{
		if (intsArray.IsCreated)
		{

			intsArray.Dispose();
		}
	}

	public struct MyTransfromJob : IJobParallelForTransform
	{
		public float deltaTime;
		public void Execute(int index, TransformAccess transform)
		{
			transform.rotation =  Quaternion.Euler(transform.rotation.eulerAngles + Vector3.up * deltaTime);
		}
	}
}
