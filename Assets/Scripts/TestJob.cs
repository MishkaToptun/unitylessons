﻿using Unity.Jobs;
using Unity.Collections;
using UnityEngine;

public partial class MyJobs
{
	public struct TestJob : IJob
	{
		public NativeArray<int> jobArray;
		public void Execute()
		{
			for (int i = 0; i < jobArray.Length; i++)
			{
				if (jobArray[i] > 10)
				{
					var oldValue = jobArray[i];
					jobArray[i] = 0;
					Debug.LogWarning($"old value: {oldValue} new value: {jobArray[i]}");
				}
				else
				{
					Debug.Log(jobArray[i]);
				}

			}
		}
	}
}
