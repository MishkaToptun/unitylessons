using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Threading;

public class Unit : MonoBehaviour
{
	// Start is called before the first frame update
	[SerializeField]
	int health = 100;
	void Start()
	{

	}

	private Coroutine current;
	public void RecieveHealth()
	{
		if (isHealthing == false)
			current = StartCoroutine(Health());

	}

	private bool isHealthing = false;
	private IEnumerator Health()
	{
		isHealthing = true;
		StartCoroutine(HealthTick());
		yield return new WaitForSeconds(3F);
		Debug.Log("stopped");
		isHealthing = false;
	}
	private IEnumerator HealthTick()
	{
		if (health < 100)
		{
			health += 5;
			Debug.Log($"healthing... hp: {health}");

			if (health > 100)
			{
				health = 100;
				yield break;
			}
		}
		yield return new WaitForSeconds(0.5F);

		if (isHealthing == true)
		{
			StartCoroutine(HealthTick());
		}
	}
	public async void TestTask()
	{

		using (CancellationTokenSource source = new CancellationTokenSource())
		{
			var token = source.Token;
			var task1 = Task1(token);
			var task2 = Task2(token, 0, 60);
			var task = await Task.WhenAny(task1, task2);
			Debug.Log(task == task1 ? "Task1" : "Task2");
			source.Cancel();
		}

	}
	private async Task Task1(CancellationToken token)
	{
		if (token.IsCancellationRequested)
		{
			Debug.Log("Task 1 canceled");

		}
		else
		{
			await Task.Delay(1000,token);
			Debug.Log("Task1 finished");
		}

	}
	private async Task Task2(CancellationToken token, int count, int maxCount)
	{
		if (token.IsCancellationRequested)
		{
			Debug.Log("Task 2 canceled");
		}
		else
		{
			await Task.Yield();
			if (count < maxCount)
			{
				await Task2(token, count + 1, maxCount);
			}
			else
			{
				Debug.Log("Task 2 finished");
			}
		}
	}
	public async void TestFaster()
	{

		using (CancellationTokenSource source = new CancellationTokenSource())
		{
			var token = source.Token;
			var task1 = Task1(token);
			var task2 = Task2(token, 0, 60);
			var result = await WhatTaskFasterAsync(token, task1, task2,source);
			Debug.Log(result);
			//source.Cancel();
		}
	}
	public static async Task<bool> WhatTaskFasterAsync(CancellationToken ct, Task task1, Task task2,CancellationTokenSource source)
	{
		var task = await Task.WhenAny(task1, task2);
		if (ct.IsCancellationRequested)
		{
			return false;
		}
		if (task == task1 & task1.IsCompleted)
		{
			source.Cancel();
			return true;
		}
		if (task == task2 & task2.IsCompleted)
		{
			source.Cancel();
			return false;
		}
		return false;

	}

}

