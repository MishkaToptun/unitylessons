﻿using UnityEngine;
using Unity.Jobs;
using Unity.Collections;

public partial class MyJobs
{
	public struct MyVectorJob : IJobParallelFor
	{
		[ReadOnly]
		public NativeArray<Vector3> positions;
		[ReadOnly]
		public NativeArray<Vector3> velocities;
		public NativeArray<Vector3> finalPositions;
		public void Execute(int index)
		{
			finalPositions[index] = positions[index] + velocities[index];
		}
	}
}
